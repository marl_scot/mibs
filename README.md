This is a simple MIB repo that I have setup
to speed up configuring SNMP get/walking on
new server monitors.
I would suggest running the following :

cd /etc/snmp/
git clone https://marl_scot@bitbucket.org/marl_scot/mibs.git

And then adding the following entry to the /etc/snmp/snmp.conf file

mibdirs +/etc/snmp/mibs/


